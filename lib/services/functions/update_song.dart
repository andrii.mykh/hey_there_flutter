int update_song(int r, int g, int b)
{
  if((r-g).abs()<25 && (r-b).abs()<25 && (g-b).abs()<25)
    return 0;
  if((r-g).abs()<25 && r-b>25 && g-b>25)
    return 1;
  if((r-b).abs()<25 && r-g>25 && b-g>25)
    return 2;
  if((g-b).abs()<25 && g-r>25 && b-r>25)
    return 3;
  if(r>g && r>b) {
    if (r > g + b)
      return 4;
    return 5;
  }
  if(g>r && g>b) {
    if (g > r + b)
      return 6;
    return 7;
  }
  if(b>r && b>g){
    if(b > r + g)
      return 8;
    return 9;
  }
  return 10;
}