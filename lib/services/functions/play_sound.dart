import 'package:audioplayer/audioplayer.dart';

void play_sound(String uri){
  AudioPlayer player = new AudioPlayer();
  player.stop();
  player.play(uri);
}