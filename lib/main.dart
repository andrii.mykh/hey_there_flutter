import 'package:flutter/material.dart';
import 'package:hey_there/pages/home.dart';

void main() => runApp(MaterialApp(
  initialRoute: '/home', //Warning: When using initialRoute, don’t define a home property.
  routes: {
    '/home': (context) => Home(),
  },
));