import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hey_there/services/functions/play_sound.dart';
import 'package:hey_there/services/functions/update_song.dart';
import 'dart:math';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  int r = 0;
  int g = 0;
  int b = 0;
  Random random = new Random();

  bool enabled = false;

  int song = 0;
  List<String> URIs = new List<String>();
  void load_songs()async{
    for(int i = 0; i<11;i++){
      final ByteData data = await rootBundle.load('music/${i}.mp3');
      Directory tempDr = await getApplicationDocumentsDirectory();
      File tempF = File("${tempDr.path}/${i}.mp3");
      await tempF.writeAsBytes(data.buffer.asUint8List(),flush: true);
      URIs.add(tempF.uri.toString());
    }
    play_sound(URIs[song]);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    load_songs();
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(body:
        InkWell(
          onTapDown: (TapDownDetails details){
            setState(() {
              enabled = true;
            });
          },
          onTapCancel: (){
            setState(() {
              enabled = false;
            });
          },
          onTap: (){
              r = random.nextInt(256);
              g = random.nextInt(256);
              b = random.nextInt(256);
              song = update_song(r,g,b);
              play_sound(URIs[song]);
          },
            child: Container(
              alignment: Alignment.center,
              color: Color.fromRGBO(r, g, b, 1.0),
              child: Text("Hey There", style:
              TextStyle(
                  color: Color.fromRGBO(255-r, 255-g, 255-b, 1.0),
                  fontSize: 60.0
              ),),
            ),
        )
      );
  }
}
